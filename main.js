//Метод это функция которая храниться как свойство объекта. Метод это действие,которое можно выполнить
//с объектами

function createNewUser() {
  let userName = {
    firstName: prompt("Введите ваше имя :"),
    lastName: prompt("Введите вашу фамилию :"),

    getLogin: function () {
      return (
        this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase()
      );
    },
  };
  return userName;
}

user1 = createNewUser();

console.log(user1);
console.log("Ваш логин :" + user1.getLogin());
